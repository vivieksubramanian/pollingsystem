<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Polling</title>
<link rel="stylesheet" type="text/css" href="style/styles.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<script src="js/functions.js"></script> 
 <script src="js/jquery.js"></script>
</head>
<body class="login">
<div style="height: 60px;width:100%;background-color: #7B7D7D;">

<div>
	<h1 id="heading">VOTE YOUR FAVOURITE HACKER</h1>
	<form action="AdminServlet" class="logout">
				<input type="hidden" class="form-control" placeholder="Eg: abc@gmail.com" name="method-name" size=40  value="logout"/>
				<input type="submit" class="btn btn-group btn-primary" value="Logout" style="position: absolute;right: 1%;top: 10px;"/>
	</form>
</div>
</div>
<div id="message" class="messageDiv"></div>
<div class="container">
<div class="row" style="position: absolute;top: 15%;width: 65%;">
<div class="col-6" >
<div class="lefttable">
	<h1 style="font-family:times new roman">CANDIDATES</h1>
	<table id="cTab" class="table table-condensed table-hover" style="width:500px;">
	<thead>
	<tr>
		<th>CANDIDATE_ID</th>
		<th>NAME</th>
		<th>VOTE_COUNT</th>
		
	</tr>
	</thead>
	<tbody>
		
	</tbody>
	</table>
</div>
</div>
<div class="col-6 ">
<div class="righttable">
	<h1 style="font-family:times new roman">CANDIDATE DETAILS</h1>
	<table id="cdTab" class="table table-condensed table-striped" style="height:100%;width:100%;">	
	<tbody></tbody>	
	</table>
</div>
</div>
</div>
</div>
</body>
<script>
userId="";
defaultIndex = 0;
getUID();
fetchAllCandidates();
function getUID()
{
	cookie = document.cookie;
	userId = cookie.split("=")[1];
}
function fetchAllCandidates()
{
	var url = "/coda_global_problem_statement/AdminServlet?method-name=fetchAllCandidates&uid="+userId;
	var cdetails = sendHttpRequest(url);
	json = JSON.parse(cdetails);
	votedfor = json.polledForUser;
	rows = json.rows;
	for(var i=0;i<rows.length;i++)
	{
		var row = rows[i];
		var cid = row[0];
		var name = row[1];
		if(votedfor==cid)
			votedforname=name;
		var voteCount = row[11];
		var tabRow = '<tr id="'+i+'" onclick=showCandidateDetails('+i+')><td>'+cid+'</td><td>'+name+'</td><td>'+voteCount+'</td></tr>';
		$("#cTab > tbody:last-child").append(tabRow);
		console.log(cid+" "+name);
	}
	if(rows.length !=0 )
	{
		showCandidateDetails(0);	
	}
	$("#message").html("You have voted for "+votedforname);
	
	
}
function voteCandidate(userId, cid)
{
	var url = "/coda_global_problem_statement/AdminServlet?method-name=vote&userId="+userId+"&cid="+cid;
	var resp = sendHttpRequest(url);
	alert("Successfully voted ...");
	location.reload();
}

function showCandidateDetails(index)
{
		$("#cdTab > tbody > tr").remove();
		var keys = ['','NAME', 'CHALLENGES_SOLVED', 'EXPERTISE_LEVEL','DATASTRUCTURES','ALGORITHMS','CPP','JAVA','PYTHON','OTHER_LANGUAGE','OTHER_LANGUAGE_RATING','VOTE_COUNT'];
		console.log("\n\nshowCandidates: "+rows[index]);
		var row = rows[index];
		var cid = row[0];
		for(var i=1; i<row.length; i++)
		{
			var propName = keys[i];
			var propVal = row[i];
			var tabRow = '<tr><td>'+propName+'</td><td>'+propVal+'</td></tr>';
			$("#cdTab > tbody:last-child").append(tabRow);
			
		}
		if(votedfor == 0)
		{
			var voteButton = '<tr><td  colspan="2"><a class="btn btn-lg btn-success" onclick=voteCandidate('+userId+','+cid+')>VOTE</a></td></tr>';
			$("#cdTab > tbody:last-child").append(voteButton);
		}
		$("#"+defaultIndex).removeClass("active");
		$("#"+index).addClass("active");
		defaultIndex=index;
		$("#cdTab").show();
		
}
/* function logoutCall() {
	var url="/coda_global_problem_statement/AdminServlet?method-name=logout";
	var result = sendHttpRequest(url);
	
	
} */

</script>
</html>	