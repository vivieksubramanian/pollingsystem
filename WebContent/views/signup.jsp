<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Polling</title>
<link rel="stylesheet" type="text/css" href="../style/styles.css">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
</head>
<body class="login">
<div style="height: 60px;width:100%;background-color: #7B7D7D;">
<div>
	<h1 id="heading">VOTE YOUR FAVOURITE HACKER</h1>
</div>
</div>
<div class="container">
<div class="row">

<div class="col-6" >
	<h1 id="loginHeading">SIGN UP</h1>
	<form style="position:absolute;top:125px"action="../AdminServlet" >
	
			<input type="hidden" class="form-control" placeholder="Eg: abc@gmail.com" name="method-name" size=40  value="registerUser"/>
		
			<div class="form-group">
				EMAIL:
				<input type="email" class="form-control" placeholder="Eg: abc@gmail.com" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" size=40  required/>
			</div>
			<div class="form-group">
				PASSWORD:
				<input type="password" class="form-control" placeholder="password" name="password"  size=40 required/>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-group btn-success">Register</button>
				<input type="reset" class="btn btn-group btn-primary" value="CLEAR"/> 
			</div>
		</form>
		</div>
		</div>
	</div>

</body>
</html>