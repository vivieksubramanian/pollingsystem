<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Polling</title>
<link rel="stylesheet" type="text/css" href="style/styles.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body class="login">
<div style="height: 60px;width:100%;background-color: #7B7D7D;">
<div>
	<h1 id="heading">VOTE YOUR FAVOURITE HACKER</h1>
</div>
</div>
<div class="container">
<div class="row">
<div class="col-6" >
	<h1 id="loginHeading">LOGIN</h1>
	<form style="position:absolute;top:125px" action="AdminServlet">
			<input type="hidden" class="form-control" placeholder="Eg: abc@gmail.com" name="method-name" size=40  value="login"/>
			
			<div class="form-group">
			<label>EMAIL :</label>
				<input type="email" class="form-control" placeholder="Eg: abc@gmail.com" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" size=40  required/>
			</div>
			
			<div class="form-group">
			<label>PASSWORD :</label>
				<input type="password" class="form-control" placeholder="password" name="password"  size=40 required/>
			</div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-group btn-success">LOGIN</button>
				<input type="reset" class="btn btn-group btn-primary" value="CLEAR"/> 	
				
			</div>
		</form>
		<div>
			<a style="postion:absolute;left:175px;top:297px" href="views/signup.jsp" class="btn btn-group btn-info">New User ?</a>
		</div>
		</div>
		
				

</div>
</div>
<script type="text/javascript">
	
	 var xhttp = document.XMLHttpRequest();
	 xhttp.onreadystatechange = function() {
		 if(xhttp.readystate == 4 && xhttp.status == 200) {
			 alert("Successfully Logged In");
		 }
	 }
	 xhttp.open("GET","AdminServlet.java",true);
	 xhttp.send();
</script>
</body>
</html>	