package com.polling.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.polling.handlers.Candidates;
import com.polling.handlers.Users;
/**
 * Servlet implementation class AdminServlet
 */
//@WebServlet(name = "AdminServlet", urlPatterns = { "/AdminServlet" })
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String USER_RESGITERED = "1";
	private static final String USER_ALREADY_EXIST = "2";
	private static final String WRONG_USER = "3";

	private static final String SUCCESS = "4";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String methodName = (String) request.getParameter("method-name");
		System.out.println("methodName: "+methodName);
		switch(methodName)
		{
		case "registerUser" :
		{
			try {
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				int result = Users.registerUser(email, password);
				if(result == 1)
				{
					response.addHeader("status", USER_RESGITERED);
				}
				else if(result == 2)
				{
					response.addHeader("status", USER_ALREADY_EXIST);
				}
				System.out.println("Register success");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/views/login.jsp") ;
				dispatcher.forward(request, response);
				response.addHeader("status", "success");
			}
			catch(Exception e) {
				response.addHeader("status", "error");
				e.printStackTrace();
			}
			break;
		}
		case "login":
		{
			try
			{
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				System.out.println("\n\nLogin  email: "+email+" pass: "+password+"\n\n");
				long uid = Users.validate(email,password);
				if(uid != 0)
				{
					System.out.println("Login success");
					Cookie ck = new Cookie("CODA_USER", ""+uid);
					response.addCookie(ck);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/views/candidate.jsp") ;
					dispatcher.forward(request, response);
				}
				else
				{
					System.out.println("Login Failure");
					response.addHeader("status", WRONG_USER);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/views/login.jsp") ;
					dispatcher.forward(request, response);
				}

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;
		}
		case "logout":
		{
			try
			{

				Cookie[] ck = request.getCookies();
				if(ck.length != 0)
				{

					for(Cookie cookie : ck)
					{
						if(cookie.getName().equals("CODA_USER"))
						{
							cookie.setMaxAge(0);
						}
					}
				}
				System.out.println("\n\nLogout Success\n\n");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/views/login.jsp") ;
				dispatcher.forward(request, response);
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;
		}
		case "vote":
		{
			try 
			{
				String uid = request.getParameter("userId");
				String cid = request.getParameter("cid");
				Users.voteCandidate(uid, cid);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/views/candidate.jsp") ;
				dispatcher.forward(request, response);
				
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			break;
		}
		case "fetchAllCandidates":
		{
			try
			{
				String uid = request.getParameter("uid");
				long cid = Users.votedForCandidate(Long.valueOf(uid));
				List<List<String>> list = Candidates.fetchCandidates();
				JSONObject json = new JSONObject();
				json.put("rows", list);
				json.put("polledForUser", ""+cid);
				response.getWriter().write(json.toString());
				response.addHeader("status", SUCCESS);


			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		case "delete":
		{
			try
			{
				String cid = request.getParameter("CID");
				Candidates.deleteCandidate(cid);
				response.addHeader("status", SUCCESS);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		case "addCandidate" :
		{
			try {
				String name = request.getParameter("name");
				int n_o_c_s = Integer.parseInt(request.getParameter("no_of_challenges"));
				int c_e_l = Integer.parseInt(request.getParameter("expertise_level"));
				int ds = Integer.parseInt(request.getParameter("ds"));
				int algo = Integer.parseInt(request.getParameter("algorithm"));
				int c_plus_plus = Integer.parseInt(request.getParameter("cpp"));
				int java = Integer.parseInt(request.getParameter("java"));
				int python = Integer.parseInt(request.getParameter("python"));
				String other = request.getParameter("other_lang");
				int otherRating = Integer.parseInt(request.getParameter("other_lang_rating"));

				int result = Candidates.addCandidate(name,n_o_c_s,c_e_l,ds,algo,c_plus_plus,java,python,other,otherRating);
				if(result > 0)
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/candidate.jsp") ;
					dispatcher.forward(request, response);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			break;
		}
		case "edit":
		{
			try {
				String name = request.getParameter("name");
				int n_o_c_s = Integer.parseInt(request.getParameter("no_of_challenges_solved"));
				int c_e_l = Integer.parseInt(request.getParameter("candidate_expertise_level"));
				int ds = Integer.parseInt(request.getParameter("datastructures"));
				int algo = Integer.parseInt(request.getParameter("algorithms"));
				int c_plus_plus = Integer.parseInt(request.getParameter("c_plus_plus"));
				int java = Integer.parseInt(request.getParameter("java"));
				int python = Integer.parseInt(request.getParameter("python"));
				String other = request.getParameter("other_language");
				int otherRating = Integer.parseInt(request.getParameter("other_language_rating"));

				int result = Candidates.updateCandidates(name,n_o_c_s,c_e_l,ds,algo,c_plus_plus,java,python,other,otherRating);
				if(result > 0)
				{
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/candidate.jsp") ;
					dispatcher.forward(request, response);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
