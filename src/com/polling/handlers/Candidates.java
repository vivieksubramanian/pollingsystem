package com.polling.handlers;

import java.util.List;

import com.polling.dao.DBUtil;

public class Candidates {

		
	public static List<List<String>> fetchCandidates() throws Exception
	{
		String sql = "select  * from Candidates";
		List<List<String>> list = DBUtil.getResultAsList(sql, 12);
		return list;
	}
	
	
	public static int addCandidate(String name,int n_o_c_s,int c_e_l,int ds,int algo,int c_plus_plus,int java,int python,String other,int otherRating) throws Exception
	{
		String sql = "insert into Candidates values (NAME, NO_OF_CHALLENGES_SOLVED, EXPERTISE_LEVEL, DS, ALGORITHM, CPP, JAVA, PYTHON, OTHER_LANG, OTHER_LANG_RATING, VOTE_COUNT) values ('"+name+"',"+n_o_c_s+", "+c_e_l+","+ds+","+algo+","+c_plus_plus+","+java+","+python+",'"+other+"',"+otherRating+",0";
		return DBUtil.executeUpdate(sql);
	}
	
	public static void deleteCandidate(String cid) throws Exception
	{
		String sql = "delete from Candidates where CID = "+cid;
		DBUtil.executeUpdate(sql);
	}
	
	public static int updateCandidates(String name,int n_o_c_s,int c_e_l,int ds,int algo,int c_plus_plus,int java,int python,String other,int otherRating) throws Exception
	{
		String sql = "update Candidate set NAME='"+name+"', NO_OF_CHANLLENGES_SOLVED="+n_o_c_s+", EXPERTISE_LEVEL="+c_e_l+", DS="+ds+", ALGORITHM="+algo+", CPP="+c_plus_plus+", JAVA="+java+",PYTHON="+python+", OTHER_LANG='"+other+"',OTHER_LANG_RATING="+otherRating ;
		return DBUtil.executeUpdate(sql);
	}
}
