package com.polling.handlers;

import com.polling.dao.DBUtil;

public class Users {
	
	public static long validate(String email,String pass) throws Exception
	{
		String sql = "select UID from Users where EMAIL='"+email+"' and PASSWORD='"+pass+"'";
		return DBUtil.getResultAsLong(sql);
	}
	
	public static int registerUser(String email,String pass) throws Exception
	{
		System.out.println("\n\nemail: "+email+" pass: "+pass+"\n\n");
		String sql1 = "select UID from Users where EMAIL = '"+email+"'";
		if(!DBUtil.hasResult(sql1))
		{
			String sql = "insert into Users(EMAIL, PASSWORD) values ('"+email+"', '"+pass+"')";
			String sql2 = "insert into UserToRole (UID, RID) select (select UID from Users where EMAIL='"+email+"'),  RID from Role where ROLE='G_USER';";
			DBUtil.executeUpdate(sql);
			DBUtil.executeUpdate(sql2);
			return 1;
		}
		else
		{
			return 2;
		}
		
	}
	
	public static long getUserId(String email) throws Exception
	{
		String userIdSql = "select UID from Users where EMAIL = '"+email+"'";
		return DBUtil.getResultAsLong(userIdSql);
		
	}
	
	public static long votedForCandidate(long uid) throws Exception
	{
		String sql = "select CID from Voting where UID = "+uid;
		return DBUtil.getResultAsLong(sql);
	}
	
	public static void voteCandidate(String uid, String cid) throws Exception
	{
		String sql = "insert into Voting values ("+uid+", "+cid+")";
		String updateSql = "update Candidates set VOTE_COUNT = (VOTE_COUNT+1) where CID = "+cid;
		DBUtil.executeUpdate(sql);
		DBUtil.executeUpdate(updateSql);
	}

}